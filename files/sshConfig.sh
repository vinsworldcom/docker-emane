#!/bin/bash

ssh-keygen -f /etc/ssh/ssh_host_rsa_key -N '' -t rsa
ssh-keygen -f /etc/ssh/ssh_host_ecdsa_key -N '' -t ecdsa -b 521
ssh-keygen -f /etc/ssh/ssh_host_ed25519_key -N '' -t ed25519 -b 521

cd /root
ssh-keygen -t rsa -N "" -f /root/.ssh/id_rsa
cd .ssh
cp id_rsa.pub authorized_keys
chmod 600 authorized_keys
touch known_hosts
