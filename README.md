# EMANE in Docker

This is EMANE packaged in Docker.


## Build

To create a Docker image, clone this repository.  Change directory into the
cloned repo directory.  Checkout the appropriate branch and then build:

```
cd docker-emane
git checkout BRANCH
docker build -t emane:BRANCH -f Dockerfile.BRANCH .
```

where `BRANCH` is the name of a branch (`git branch -a`).

## Run

```
docker run --privileged --name emane --hostname emane -it emane:BRANCH /bin/bash
```

You're now ready to run EMANE simulations.  Once the container is `run` from
the above command, to get back in, you'll only need:

```
docker start -i emane
```

---

Copyright (c) - 2024 The MITRE Corporation. All Rights Reserved.

Public release case number:  24-1190
